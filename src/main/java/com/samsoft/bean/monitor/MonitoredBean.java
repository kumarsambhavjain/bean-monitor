package com.samsoft.bean.monitor;

/**
 * Marker interface to be implemented by beans that needs monitoring
 * functionality.
 * 
 * @author Kumar Sambhav Jain
 * 
 */
public interface MonitoredBean {

}
