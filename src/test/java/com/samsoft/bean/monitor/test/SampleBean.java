/**
 * 
 */
package com.samsoft.bean.monitor.test;

import com.samsoft.bean.monitor.annotation.Monitored;

/**
 * Sample JavaBean
 * 
 * @author kjai10
 * 
 */
@Monitored
public class SampleBean {

	private boolean flag;
	private String message;
	private String code;

	/**
	 * @return the flag
	 */
	public boolean isFlag() {
		return flag;
	}

	/**
	 * @param flag
	 *            the flag to set
	 */
	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

}
