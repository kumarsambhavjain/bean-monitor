/**
 * 
 */
package com.samsoft.bean.monitor.test;

import com.samsoft.bean.monitor.annotation.Monitored;

/**
 * @author kjai10
 * 
 */
@Monitored
public class AnotherSampleBean {

	private Double d345;
	private boolean isFine;

	/**
	 * @return the d345
	 */
	public Double getD345() {
		return d345;
	}

	/**
	 * @param d345
	 *            the d345 to set
	 */
	public void setD345(Double d345) {
		this.d345 = d345;
	}

	/**
	 * @return the isFine
	 */
	public boolean isFine() {
		return isFine;
	}

	/**
	 * @param isFine
	 *            the isFine to set
	 */
	public void setFine(boolean isFine) {
		this.isFine = isFine;
	}

}
