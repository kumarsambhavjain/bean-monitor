/**
 * 
 */
package com.samsoft.bean.monitor.test;

import com.samsoft.bean.monitor.annotation.Monitored;


/**
 * @author Kumar Sambhav Jain
 * 
 */
@Monitored
public class UserDetails {

	private String name;
	private String email;
	private short age;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the age
	 */
	public short getAge() {
		return age;
	}

	/**
	 * @param age
	 *            the age to set
	 */
	public void setAge(short age) {
		this.age = age;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UserDetails [name=" + name + ", email=" + email + ", age="
				+ age + ", salary=";
	}

}
